using ECSTestWork.Component;
using Leopotam.EcsLite;

namespace ECSTestWork.System
{
    public class GroundCollisionSystem : IEcsRunSystem
    {
        public void Run(EcsSystems systems)
        {
            var groundCollisionFilter = systems.GetWorld().Filter<GroundCollisionComponent>().End();

            foreach (var groundCollision in groundCollisionFilter)
            {
                systems.GetWorld().DelEntity(groundCollision);
            }
        }
    }
}