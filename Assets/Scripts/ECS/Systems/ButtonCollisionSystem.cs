using ECSTestWork.Component;
using Leopotam.EcsLite;

namespace ECSTestWork.System
{
    public class ButtonCollisionSystem : IEcsRunSystem
    {
        public void Run(EcsSystems systems)
        {
            var buttonCollisionFilter = systems.GetWorld().Filter<ButtonCollisionComponent>().End();
            var buttonCollisionPool = systems.GetWorld().GetPool<ButtonCollisionComponent>();

            var pushButtonPool = systems.GetWorld().GetPool<PushButtonComponent>();

            var openDoorPool = systems.GetWorld().GetPool<OpenDoorComponent>();

            foreach (var buttonCollisionEntity in buttonCollisionFilter)
            {
                var buttonCollisionComponent = buttonCollisionPool.Get(buttonCollisionEntity);

                var pushButtonEntity = systems.GetWorld().NewEntity();
                ref var pushButtonComponent = ref pushButtonPool.Add(pushButtonEntity);
                pushButtonComponent.ButtonId = buttonCollisionComponent.ButtonId;

                var configuratorMask = systems.GetWorld().Filter<ConfiguratorComponent>().End();
                var configuratorPool = systems.GetWorld().GetPool<ConfiguratorComponent>();

                foreach (var configuratorEntity in configuratorMask)
                {
                    var configuratorComponent = configuratorPool.Get(configuratorEntity);

                    foreach (var listener in configuratorComponent.Value
                                 .GetComponentListeners<PushButtonComponent>())
                    {
                        listener.Invoke(pushButtonComponent);
                    }
                }

                foreach (var connectedDoorId in buttonCollisionComponent.ConnectedDoorIds)
                {
                    var openDoorEntity = systems.GetWorld().NewEntity();
                    ref var openDoorComponent = ref openDoorPool.Add(openDoorEntity);
                    openDoorComponent.DoorId = connectedDoorId;

                    foreach (var configuratorEntity in configuratorMask)
                    {
                        var configuratorComponent = configuratorPool.Get(configuratorEntity);

                        foreach (var listener in configuratorComponent.Value
                                     .GetComponentListeners<OpenDoorComponent>())
                        {
                            listener.Invoke(openDoorComponent);
                        }
                    }
                }

                systems.GetWorld().DelEntity(buttonCollisionEntity);
            }
        }
    }
}