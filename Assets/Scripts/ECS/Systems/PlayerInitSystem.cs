using ECSTestWork.Component;
using Leopotam.EcsLite;

namespace ECSTestWork.System
{
	public class PlayerInitSystem : IEcsInitSystem
	{
		public void Init(EcsSystems systems)
		{
			var configuratorEntity = systems.GetWorld().NewEntity();
			ref var configuratorComponent =
				ref systems.GetWorld().GetPool<ConfiguratorComponent>().Add(configuratorEntity);

			configuratorComponent.Value = systems.GetShared<SharedData>().ConfiguratorService;

			var playerEntity = systems.GetWorld().NewEntity();
			ref var inputComponent = ref systems.GetWorld().GetPool<InputComponent>().Add(playerEntity);
			inputComponent.Value = configuratorComponent.Value.GetInputService();
			ref var movableComponent = ref systems.GetWorld().GetPool<MovableComponent>().Add(playerEntity);
			movableComponent.Value = configuratorComponent.Value.GetMovableService();
		}
	}
}