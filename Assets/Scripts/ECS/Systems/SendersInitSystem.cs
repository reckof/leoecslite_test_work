using ECSTestWork.Component;
using Leopotam.EcsLite;

namespace ECSTestWork.System
{
    public class SendersInitSystem : IEcsInitSystem
    {
        public void Init(EcsSystems systems)
        {
            var configuratorFilter = systems.GetWorld().Filter<ConfiguratorComponent>().End();
            var configuratorPool = systems.GetWorld().GetPool<ConfiguratorComponent>();

            foreach (var configuratorEntity in configuratorFilter)
            {
                var configuratorComponent = configuratorPool.Get(configuratorEntity);

                foreach (var buttonCollisionSender in configuratorComponent.Value
                             .GetComponentSenders<ButtonCollisionComponent>())
                {
                    buttonCollisionSender.OnSend += delegate(ButtonCollisionComponent component)
                    {
                        ButtonCollisionHandler(component, systems);
                    };
                }

                foreach (var groundCollisionSender in configuratorComponent.Value
                             .GetComponentSenders<GroundCollisionComponent>())
                {
                    groundCollisionSender.OnSend += delegate(GroundCollisionComponent component)
                    {
                        GroundCollisionHandler(component, systems);
                    };
                }

                foreach (var otherCollisionSender in configuratorComponent.Value
                             .GetComponentSenders<OtherCollisionComponent>())
                {
                    otherCollisionSender.OnSend += delegate(OtherCollisionComponent component)
                    {
                        OtherCollisionHandler(component, systems);
                    };
                }
            }
        }

        private void ButtonCollisionHandler(ButtonCollisionComponent component, EcsSystems systems)
        {
            var buttonCollisionEntity = systems.GetWorld().NewEntity();
            var buttonCollisionPool = systems.GetWorld().GetPool<ButtonCollisionComponent>();
            ref var buttonCollisionComponent = ref buttonCollisionPool.Add(buttonCollisionEntity);
            buttonCollisionComponent.ButtonId = component.ButtonId;
            buttonCollisionComponent.ConnectedDoorIds = component.ConnectedDoorIds;
        }

        private void GroundCollisionHandler(GroundCollisionComponent component, EcsSystems systems)
        {
            var groundCollisionEntity = systems.GetWorld().NewEntity();
            var groundCollisionPool = systems.GetWorld().GetPool<GroundCollisionComponent>();
            ref var groundCollisionComponent = ref groundCollisionPool.Add(groundCollisionEntity);
        }

        private void OtherCollisionHandler(OtherCollisionComponent component, EcsSystems systems)
        {
            var otherCollisionEntity = systems.GetWorld().NewEntity();
            var otherCollisionPool = systems.GetWorld().GetPool<OtherCollisionComponent>();
            ref var otherCollisionComponent = ref otherCollisionPool.Add(otherCollisionEntity);
        }
    }
}