using ECSTestWork.Component;
using Leopotam.EcsLite;

namespace ECSTestWork.System
{
    public class InputReactSystem : IEcsRunSystem
    {
        public void Run(EcsSystems systems)
        {
            var movableFilter = systems.GetWorld().Filter<MovableComponent>().End();
            var movablePool = systems.GetWorld().GetPool<MovableComponent>();

            var movingFilter = systems.GetWorld().Filter<MovingComponent>().End();
            var movingPool = systems.GetWorld().GetPool<MovingComponent>();

            var stoppedMovingFilter = systems.GetWorld().Filter<StoppedMovingComponent>().End();
            var stoppedMovingPool = systems.GetWorld().GetPool<StoppedMovingComponent>();

            var inputValueFilter = systems.GetWorld().Filter<InputValueComponent>().End();
            var inputValuePool = systems.GetWorld().GetPool<InputValueComponent>();

            foreach (var inputValueEntity in inputValueFilter)
            {
                foreach (var movableEntity in movableFilter)
                {
                    stoppedMovingPool.Del(movableEntity);
                    movingPool.Del(movableEntity);

                    ref var movableComponent = ref movablePool.Get(movableEntity);
                    movableComponent.Value.SetCurrentPoint(inputValuePool.Get(inputValueEntity).Value);

                    ref var movingComponent = ref movingPool.Add(movableEntity);

                    var configuratorMask = systems.GetWorld().Filter<ConfiguratorComponent>().End();
                    var configuratorPool = systems.GetWorld().GetPool<ConfiguratorComponent>();

                    foreach (var configuratorEntity in configuratorMask)
                    {
                        var configuratorComponent = configuratorPool.Get(configuratorEntity);

                        foreach (var listener in configuratorComponent.Value
                                     .GetComponentListeners<MovingComponent>())
                        {
                            listener.Invoke(movingComponent);
                        }
                    }
                }

                systems.GetWorld().DelEntity(inputValueEntity);
            }
        }
    }
}