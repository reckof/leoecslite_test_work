using System;
using ECSTestWork.Component;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECSTestWork.System
{
    public class OtherCollisionSystem : IEcsRunSystem
    {
        public void Run(EcsSystems systems)
        {
            var otherCollisionFilter = systems.GetWorld().Filter<OtherCollisionComponent>().End();
            var otherCollisionPool = systems.GetWorld().GetPool<OtherCollisionComponent>();

            var movableFilter = systems.GetWorld().Filter<MovableComponent>().End();
            var movablePool = systems.GetWorld().GetPool<MovableComponent>();

            var movingFilter = systems.GetWorld().Filter<MovingComponent>().End();
            var movingPool = systems.GetWorld().GetPool<MovingComponent>();

            var stoppedMovingFilter = systems.GetWorld().Filter<StoppedMovingComponent>().End();
            var stoppedMovingPool = systems.GetWorld().GetPool<StoppedMovingComponent>();

            foreach (var otherCollisionEntity in otherCollisionFilter)
            {
                foreach (var movableEntity in movableFilter)
                {
                    movingPool.Del(movableEntity);
                    try
                    {
                        ref var stoppedMovingComponent = ref stoppedMovingPool.Add(movableEntity);

                        var configuratorMask = systems.GetWorld().Filter<ConfiguratorComponent>().End();
                        var configuratorPool = systems.GetWorld().GetPool<ConfiguratorComponent>();

                        foreach (var configuratorEntity in configuratorMask)
                        {
                            var configuratorComponent = configuratorPool.Get(configuratorEntity);

                            foreach (var listener in configuratorComponent.Value
                                         .GetComponentListeners<StoppedMovingComponent>())
                            {
                                listener.Invoke(stoppedMovingComponent);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e);
                    }
                }

                systems.GetWorld().DelEntity(otherCollisionEntity);
            }
        }
    }
}