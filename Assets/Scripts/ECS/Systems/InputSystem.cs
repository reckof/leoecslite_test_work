using ECSTestWork.Component;
using Leopotam.EcsLite;

namespace ECSTestWork.System
{
    public class InputSystem : IEcsRunSystem
    {
        public void Run(EcsSystems systems)
        {
            var inputFilter = systems.GetWorld().Filter<InputComponent>().End();
            var inputPool = systems.GetWorld().GetPool<InputComponent>();
            var inputValueFilter = systems.GetWorld().Filter<InputValueComponent>().End();
            var inputValuePool = systems.GetWorld().GetPool<InputValueComponent>();

            foreach (var inputEntity in inputFilter)
            {
                ref var inputComponent = ref inputPool.Get(inputEntity);
                if (inputComponent.Value.IsInputValidate() && inputComponent.Value.GetInputPosition().HasValue)
                {
                    foreach (var inputValueEntity in inputValueFilter)
                    {
                        systems.GetWorld().DelEntity(inputValueEntity);
                    }

                    var newInputValueEntity = systems.GetWorld().NewEntity();
                    ref var newInputValueComponent = ref inputValuePool.Add(newInputValueEntity);
                    newInputValueComponent.Value = inputComponent.Value.GetInputPosition().Value;

                    var configuratorMask = systems.GetWorld().Filter<ConfiguratorComponent>().End();
                    var configuratorPool = systems.GetWorld().GetPool<ConfiguratorComponent>();

                    foreach (var configuratorEntity in configuratorMask)
                    {
                        var configuratorComponent = configuratorPool.Get(configuratorEntity);

                        foreach (var listener in configuratorComponent.Value
                                     .GetComponentListeners<InputValueComponent>())
                        {
                            listener.Invoke(newInputValueComponent);
                        }
                    }
                }
            }
        }
    }
}