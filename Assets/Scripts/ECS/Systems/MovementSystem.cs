using ECSTestWork.Component;
using Leopotam.EcsLite;

namespace ECSTestWork.System
{
    public class MovementSystem : IEcsRunSystem
    {
        public void Run(EcsSystems systems)
        {
            var movableFilter = systems.GetWorld().Filter<MovableComponent>().End();
            var movablePool = systems.GetWorld().GetPool<MovableComponent>();

            var movingFilter = systems.GetWorld().Filter<MovingComponent>().End();
            var movingPool = systems.GetWorld().GetPool<MovingComponent>();

            var stoppedMovingFilter = systems.GetWorld().Filter<StoppedMovingComponent>().End();
            var stoppedMovingPool = systems.GetWorld().GetPool<StoppedMovingComponent>();

            foreach (var movingEntity in movingFilter)
            {
                var movableComponent = movablePool.Get(movingEntity);
                if (movableComponent.Value.GetCurrentPoint() != null)
                {
                    if (movableComponent.Value.IsPathComplete())
                    {
                        movingPool.Del(movingEntity);

                        movableComponent.Value.ClearCurrentPoint();

                        ref var stoppedMovingComponent = ref stoppedMovingPool.Add(movingEntity);

                        var configuratorMask = systems.GetWorld().Filter<ConfiguratorComponent>().End();
                        var configuratorPool = systems.GetWorld().GetPool<ConfiguratorComponent>();

                        foreach (var configuratorEntity in configuratorMask)
                        {
                            var configuratorComponent = configuratorPool.Get(configuratorEntity);

                            foreach (var listener in configuratorComponent.Value
                                         .GetComponentListeners<StoppedMovingComponent>())
                            {
                                listener.Invoke(stoppedMovingComponent);
                            }
                        }
                    }
                    else
                    {
                        movableComponent.Value.Move();
                    }
                }
            }
        }
    }
}