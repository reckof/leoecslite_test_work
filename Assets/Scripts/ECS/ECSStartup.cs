using ECSTestWork.Service;
using ECSTestWork.System;
using Leopotam.EcsLite;
using UnityEngine;

namespace ECSTestWork
{
	public class ECSStartup : MonoBehaviour
	{
		[SerializeField] private UnityConfiguratorService _configuratorService;

		private EcsWorld _world;
		private EcsSystems _updateSystems;
		private EcsSystems _fixedUpdateSystems;

		private void Start()
		{
			CreateObjects();
			InitializeUpdateSystems();
			InitializeFixedUpdateSystems();
		}

		private void Update()
		{
			_updateSystems.Run();
		}

		private void FixedUpdate()
		{
			_fixedUpdateSystems.Run();
		}

		private void OnDestroy()
		{
			_updateSystems.Destroy();
			_updateSystems = null;

			_fixedUpdateSystems.Destroy();
			_fixedUpdateSystems = null;

			_world.Destroy();
			_world = null;
		}

		private void CreateObjects()
		{
			_world = new EcsWorld();
			var sharedData = new SharedData(_configuratorService);

			_updateSystems = new EcsSystems(_world, sharedData);
			_fixedUpdateSystems = new EcsSystems(_world, sharedData);
		}

		private void InitializeUpdateSystems()
		{
			_updateSystems
				.Add(new PlayerInitSystem())
				.Add(new SendersInitSystem())
				.Add(new InputSystem())
				.Add(new InputReactSystem())
				.Add(new ButtonCollisionSystem())
				.Add(new GroundCollisionSystem())
				.Add(new OtherCollisionSystem());

			_updateSystems.Init();
		}

		private void InitializeFixedUpdateSystems()
		{
			_fixedUpdateSystems
				.Add(new MovementSystem());

			_fixedUpdateSystems.Init();
		}
	}
}