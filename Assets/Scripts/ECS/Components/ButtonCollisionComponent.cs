using System.Collections.Generic;

namespace ECSTestWork.Component
{
    public struct ButtonCollisionComponent
    {
        public string ButtonId;
        public List<string> ConnectedDoorIds;
    }
}