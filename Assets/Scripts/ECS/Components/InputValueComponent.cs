using UnityEngine;

namespace ECSTestWork.Component
{
    public struct InputValueComponent
    {
        public Vector3 Value;
    }
}