using ECSTestWork.Service;

namespace ECSTestWork.Component
{
    public struct MovableComponent
    {
        public IMovableService Value;
    }
}