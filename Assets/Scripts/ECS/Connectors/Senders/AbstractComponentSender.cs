using System;

namespace ECSTestWork.Connector.Sender
{
    public abstract class AbstractComponentSender<TComponent> : ECSWorldConnector<TComponent>,
        IComponentSender<TComponent>
        where TComponent : struct
    {
        public event Action<TComponent> OnSend = delegate(TComponent component) {  };

        protected void SendComponent(TComponent component)
        {
            OnSend(component);
        }
    }

    public interface IComponentSender<out TComponent> : IComponentSender
    {
        event Action<TComponent> OnSend;
    }

    public interface IComponentSender
    {
    }
}