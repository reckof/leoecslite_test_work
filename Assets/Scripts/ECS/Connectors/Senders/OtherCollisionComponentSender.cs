using ECSTestWork.Component;
using ECSTestWork.View;
using UnityEngine;

namespace ECSTestWork.Connector.Sender
{
    public class OtherCollisionComponentSender : AbstractComponentSender<OtherCollisionComponent>
    {
        [SerializeField] private PlayerView _player;

        private void Awake()
        {
            _player.OnOtherCollision += OnOtherCollisionHandler;
        }

        private void OnOtherCollisionHandler(GameObject go)
        {
            var otherCollisionComponent = new OtherCollisionComponent();
            SendComponent(otherCollisionComponent);
        }
    }
}