using System.Linq;
using ECSTestWork.Component;
using ECSTestWork.View;
using UnityEngine;

namespace ECSTestWork.Connector.Sender
{
    public class ButtonCollisionComponentSender : AbstractComponentSender<ButtonCollisionComponent>
    {
        [SerializeField] private PlayerView _player;

        private void Awake()
        {
            _player.OnButtonCollision += OnButtonCollisionHandler;
        }

        private void OnButtonCollisionHandler(ButtonView buttonView)
        {
            var buttonCollisionComponent = new ButtonCollisionComponent
            {
                ButtonId = buttonView.Config.ID,
                ConnectedDoorIds = buttonView.Config.ConnectedDoors.Select(e => e.ID).ToList()
            };
            
            SendComponent(buttonCollisionComponent);
        }
    }
}