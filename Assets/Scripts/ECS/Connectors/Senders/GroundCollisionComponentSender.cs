using ECSTestWork.Component;
using ECSTestWork.View;
using UnityEngine;

namespace ECSTestWork.Connector.Sender
{
    public class GroundCollisionComponentSender : AbstractComponentSender<GroundCollisionComponent>
    {
        [SerializeField] private PlayerView _player;

        private void Awake()
        {
            _player.OnGroundCollision += OnGroundCollisionHandler;
        }

        private void OnGroundCollisionHandler()
        {
            var groundCollisionComponent = new GroundCollisionComponent();
            SendComponent(groundCollisionComponent);
        }
    }
}