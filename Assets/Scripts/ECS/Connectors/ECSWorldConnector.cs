using System;
using UnityEngine;

namespace ECSTestWork.Connector
{
    public abstract class ECSWorldConnector : MonoBehaviour
    {
        public abstract Type ComponentType { get; }
    }

    public abstract class ECSWorldConnector<TComponent> : ECSWorldConnector where TComponent : struct
    {
        public override Type ComponentType => typeof(TComponent);
    }
}