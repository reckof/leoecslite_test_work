using System.Collections.Generic;
using System.Linq;
using ECSTestWork.Component;
using ECSTestWork.View;
using UnityEngine;

namespace ECSTestWork.Connector.Listener
{
    public class PushButtonComponentListener : AbstractComponentListener<PushButtonComponent>
    {
        [SerializeField] private List<ButtonView> _buttons;

        public override void Invoke(PushButtonComponent component)
        {
            var buttonView = _buttons.FirstOrDefault(e => e.Config.ID.Equals(component.ButtonId));
            if (buttonView != null)
            {
                buttonView.Push();
            }
            else
            {
                Debug.Log("Can't found button with id " + component.ButtonId);
            }
        }
    }
}