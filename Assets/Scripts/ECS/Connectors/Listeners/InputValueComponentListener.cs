using ECSTestWork.Component;
using ECSTestWork.View;
using UnityEngine;

namespace ECSTestWork.Connector.Listener
{
    public class InputValueComponentListener : AbstractComponentListener<InputValueComponent>
    {
        [SerializeField] private CursorView _cursor;

        public override void Invoke(InputValueComponent component)
        {
            _cursor.ShowInPosition(component.Value);
        }
    }
}