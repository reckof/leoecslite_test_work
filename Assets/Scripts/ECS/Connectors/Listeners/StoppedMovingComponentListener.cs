using ECSTestWork.Component;
using ECSTestWork.View;
using UnityEngine;

namespace ECSTestWork.Connector.Listener
{
    public class StoppedMovingComponentListener : AbstractComponentListener<StoppedMovingComponent>
    {
        [SerializeField] private PlayerView _player;
        [SerializeField] private CursorView _cursor;

        public override void Invoke(StoppedMovingComponent component)
        {
            _player.PlayIdle();
            _cursor.Hide();
        }
    }
}