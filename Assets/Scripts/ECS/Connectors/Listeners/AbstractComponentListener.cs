namespace ECSTestWork.Connector.Listener
{
    public abstract class AbstractComponentListener<TComponent> : ECSWorldConnector<TComponent>,
        IComponentListener<TComponent>
        where TComponent : struct
    {
        public abstract void Invoke(TComponent component);
    }

    public interface IComponentListener<in TComponent> : IComponentListener
    {
        void Invoke(TComponent component);
    }

    public interface IComponentListener
    {
    }
}