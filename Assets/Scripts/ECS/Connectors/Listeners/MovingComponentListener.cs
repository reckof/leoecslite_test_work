using ECSTestWork.Component;
using ECSTestWork.View;
using UnityEngine;

namespace ECSTestWork.Connector.Listener
{
    public class MovingComponentListener : AbstractComponentListener<MovingComponent>
    {
        [SerializeField] private PlayerView _player;

        public override void Invoke(MovingComponent component)
        {
            _player.PlayMove();
        }
    }
}