using System.Collections.Generic;
using System.Linq;
using ECSTestWork.Component;
using ECSTestWork.View;
using UnityEngine;

namespace ECSTestWork.Connector.Listener
{
    public class OpenDoorComponentListener : AbstractComponentListener<OpenDoorComponent>
    {
        [SerializeField] private List<DoorView> _doors;

        public override void Invoke(OpenDoorComponent component)
        {
            var doorView = _doors.FirstOrDefault(e => e.Config.ID.Equals(component.DoorId));
            if (doorView != null)
            {
                doorView.Open();
            }
            else
            {
                Debug.Log("Can't found door with id " + component.DoorId);
            }
        }
    }
}