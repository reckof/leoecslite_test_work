using System.Collections;
using UnityEngine;

namespace ECSTestWork.View
{
    public class AnimationView : MonoBehaviour
    {
        private Coroutine _animationCoroutine;

        public void ShowAnimation(Transform animatedTransform, Transform endPositionTransform,
            float animationTime)
        {
            if (_animationCoroutine != null)
            {
                StopCoroutine(_animationCoroutine);
            }

            _animationCoroutine =
                StartCoroutine(AnimationCoroutine(animatedTransform, endPositionTransform, animationTime));
        }

        private IEnumerator AnimationCoroutine(Transform animatedTransform, Transform endPositionTransform,
            float animationTime)
        {
            var startPos = animatedTransform.position;
            var endPos = endPositionTransform.position;

            var t = 0f;

            while (t <= 1f)
            {
                animatedTransform.position = Vector3.Lerp(startPos, endPos, t);
                t += Time.deltaTime / animationTime;
                yield return null;
            }

            animatedTransform.position = endPos;
        }
    }
}