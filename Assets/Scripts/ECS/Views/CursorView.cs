using UnityEngine;

namespace ECSTestWork.View
{
    public class CursorView : MonoBehaviour
    {
        [SerializeField] private MeshRenderer _renderer;

        private void Awake()
        {
            Hide();
        }

        public void ShowInPosition(Vector3 position)
        {
            transform.position = position;
            _renderer.enabled = true;
        }

        public void Hide()
        {
            _renderer.enabled = false;
        }
    }
}