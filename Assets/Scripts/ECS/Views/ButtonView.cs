using ECSTestWork.Config;
using UnityEngine;

namespace ECSTestWork.View
{
    public class ButtonView : MonoBehaviour
    {
        [SerializeField] private ButtonConfig _config;
        [SerializeField] private Transform _pivot;
        [SerializeField] private Transform _unPressedTransfrom;
        [SerializeField] private Transform _pressedTransform;
        [SerializeField] private float _animationTime;
        [SerializeField] private AnimationView _animationView;

        public ButtonConfig Config => _config;

        private void Awake()
        {
            UnPush();
        }

        public void Push()
        {
            _animationView.ShowAnimation(_pivot, _pressedTransform, _animationTime);
        }

        public void UnPush()
        {
            _animationView.ShowAnimation(_pivot, _unPressedTransfrom, _animationTime);
        }
    }
}