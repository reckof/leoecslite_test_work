using ECSTestWork.Config;
using UnityEngine;

namespace ECSTestWork.View
{
    public class DoorView : MonoBehaviour
    {
        [SerializeField] private DoorConfig _config;
        [SerializeField] private Transform _pivot;
        [SerializeField] private Transform _openTransform;
        [SerializeField] private Transform _closeTransform;
        [SerializeField] private float _animationTime;
        [SerializeField] private AnimationView _animationView;

        public DoorConfig Config => _config;

        private void Awake()
        {
            Close();
        }

        public void Open()
        {
            _animationView.ShowAnimation(_pivot, _openTransform, _animationTime);
        }

        public void Close()
        {
            _animationView.ShowAnimation(_pivot, _closeTransform, _animationTime);
        }
    }
}