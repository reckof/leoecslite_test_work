using System;
using ECSTestWork.Config;
using UnityEngine;

namespace ECSTestWork.View
{
    public class PlayerView : MonoBehaviour
    {
        [SerializeField] private PlayerConfig _config;
        [SerializeField] private Animator _animator;

        public PlayerConfig Config => _config;

        public event Action<ButtonView> OnButtonCollision = delegate(ButtonView buttonView) { };
        public event Action OnGroundCollision = delegate { };
        public event Action<GameObject> OnOtherCollision = delegate(GameObject go) { };

        private void OnCollisionEnter(Collision collision)
        {
            var buttonView = collision.gameObject.GetComponent<ButtonView>();
            if (buttonView != null)
            {
                OnButtonCollision(buttonView);
                return;
            }

            var groundView = collision.gameObject.GetComponent<GroundView>();
            if (groundView != null)
            {
                OnGroundCollision();
                return;
            }

            OnOtherCollision(collision.gameObject);
        }

        public void PlayMove()
        {
            _animator.SetBool(_config.MoveAnimationBoolName, true);
        }

        public void PlayIdle()
        {
            _animator.SetBool(_config.MoveAnimationBoolName, false);
        }
    }
}