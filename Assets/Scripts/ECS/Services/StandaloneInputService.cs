using UnityEngine;

namespace ECSTestWork.Service
{
    public class StandaloneInputService : IInputService
    {
        private Camera _camera;

        public StandaloneInputService(Camera camera)
        {
            _camera = camera;
        }

        public bool IsInputValidate()
        {
            return Input.GetMouseButtonDown(0);
        }

        public Vector3? GetInputPosition()
        {
            var ray = _camera.ScreenPointToRay(Input.mousePosition);
            Vector3? hitPoint = null;

            if (Physics.Raycast(ray, out var hit))
            {
                hitPoint = hit.point;
            }

            return hitPoint;
        }
    }
}