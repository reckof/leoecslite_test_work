using System.Collections.Generic;
using ECSTestWork.Connector.Listener;
using ECSTestWork.Connector.Sender;

namespace ECSTestWork.Service
{
    public interface IConfiguratorService
    {
        IInputService GetInputService();
        IMovableService GetMovableService();

        public IEnumerable<IComponentListener<TComponent>> GetComponentListeners<TComponent>()
            where TComponent : struct;

        public IEnumerable<IComponentSender<TComponent>> GetComponentSenders<TComponent>()
            where TComponent : struct;
    }
}