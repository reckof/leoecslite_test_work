using UnityEngine;

namespace ECSTestWork.Service
{
    public interface IMovableService
    {
        public void SetCurrentPoint(Vector3 point);
        public void ClearCurrentPoint();
        public Vector3? GetCurrentPoint();
        public void Move();
        public bool IsPathComplete();
    }
}