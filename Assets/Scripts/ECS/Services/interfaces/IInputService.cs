using UnityEngine;

namespace ECSTestWork.Service
{
    public interface IInputService
    {
        bool IsInputValidate();
        Vector3? GetInputPosition();
    }
}