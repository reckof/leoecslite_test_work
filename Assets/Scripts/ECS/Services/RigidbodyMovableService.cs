using UnityEngine;

namespace ECSTestWork.Service
{
    public class RigidbodyMovableService : IMovableService
    {
        private Rigidbody _rigidbody;
        private float _speed;
        private float _stopDistance;
        private Vector3? _currentPoint;

        public RigidbodyMovableService(Rigidbody rigidbody, float speed, float stopDistance)
        {
            _rigidbody = rigidbody;
            _speed = speed;
            _stopDistance = stopDistance;
        }

        public void SetCurrentPoint(Vector3 point)
        {
            _currentPoint = point;
        }

        public void ClearCurrentPoint()
        {
            _currentPoint = null;
        }

        public Vector3? GetCurrentPoint()
        {
            return _currentPoint;
        }

        public void Move()
        {
            if (!_currentPoint.HasValue) return;

            _rigidbody.MovePosition(_rigidbody.position + _rigidbody.transform.forward * Time.deltaTime * _speed);
            var lookDirection = _currentPoint.Value - _rigidbody.position;
            lookDirection.Normalize();

            _rigidbody.MoveRotation(Quaternion.Slerp(_rigidbody.rotation, Quaternion.LookRotation(lookDirection),
                _speed * Time.deltaTime));
        }

        public bool IsPathComplete()
        {
            if (_currentPoint.HasValue)
            {
                return Vector3.Distance(_currentPoint.Value, _rigidbody.position) <= _stopDistance;
            }

            return false;
        }
    }
}