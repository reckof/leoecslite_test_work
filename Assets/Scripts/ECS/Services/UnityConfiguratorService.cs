using System.Collections.Generic;
using System.Linq;
using ECSTestWork.Connector;
using ECSTestWork.Connector.Listener;
using ECSTestWork.Connector.Sender;
using ECSTestWork.View;
using UnityEngine;

namespace ECSTestWork.Service
{
    public class UnityConfiguratorService : MonoBehaviour, IConfiguratorService
    {
        [SerializeField] private PlayerView _player;
        [SerializeField] private Camera _raycastCamera;
        [SerializeField] private List<ECSWorldConnector> _listeners;
        [SerializeField] private List<ECSWorldConnector> _senders;

        private void OnValidate()
        {
            var listeners = _listeners.Where(e => e is IComponentListener).ToList();
            _listeners = listeners;
            var senders = _senders.Where(e => e is IComponentSender).ToList();
            _senders = senders;
        }

        public IInputService GetInputService()
        {
            return new StandaloneInputService(_raycastCamera);
        }

        public IMovableService GetMovableService()
        {
            return new RigidbodyMovableService(_player.GetComponent<Rigidbody>(), _player.Config.Speed,
                _player.Config.StopDistance);
        }

        public IEnumerable<IComponentListener<TComponent>> GetComponentListeners<TComponent>() where TComponent : struct
        {
            return _listeners.Where(e => e.ComponentType == typeof(TComponent)).Cast<IComponentListener<TComponent>>();
        }

        public IEnumerable<IComponentSender<TComponent>> GetComponentSenders<TComponent>() where TComponent : struct
        {
            return _senders.Where(e => e.ComponentType == typeof(TComponent)).Cast<IComponentSender<TComponent>>();
        }
    }
}