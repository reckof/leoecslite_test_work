using ECSTestWork.Service;

namespace ECSTestWork
{
    public class SharedData
    {
        public IConfiguratorService ConfiguratorService { get; }

        public SharedData(IConfiguratorService configuratorService)
        {
            ConfiguratorService = configuratorService;
        }
    }
}