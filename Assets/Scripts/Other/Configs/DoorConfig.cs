using UnityEngine;

namespace ECSTestWork.Config
{
    [CreateAssetMenu(fileName = "DoorConfig", menuName = "Config/DoorConfig")]
    public class DoorConfig : Config
    {
    }
}