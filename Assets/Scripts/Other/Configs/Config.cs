using UnityEngine;

namespace ECSTestWork.Config
{
    public abstract class Config : ScriptableObject
    {
        [SerializeField] private string _id;

        public string ID => _id;
    }
}