using UnityEngine;

namespace ECSTestWork.Config
{
    [CreateAssetMenu(fileName = "PlayerConfig", menuName = "Config/PlayerConfig")]
    public class PlayerConfig : Config
    {
        [SerializeField] private float _speed;
        [SerializeField] private float _stopDistance;
        [SerializeField] private string _moveAnimationBoolName;
        
        public float Speed => _speed;
        public float StopDistance => _stopDistance;
        public string MoveAnimationBoolName => _moveAnimationBoolName;
    }
}