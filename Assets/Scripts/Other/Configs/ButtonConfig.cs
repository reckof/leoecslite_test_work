using System.Collections.Generic;
using UnityEngine;

namespace ECSTestWork.Config
{
    [CreateAssetMenu(fileName = "ButtonConfig", menuName = "Config/ButtonConfig")]
    public class ButtonConfig : Config
    {
        [SerializeField] private List<DoorConfig> _connectedDoors;

        public List<DoorConfig> ConnectedDoors => _connectedDoors;
    }
}